<?php

class DateHelper
{
    /**
     * Pasa una fecha en string o array a una fecha en string, validada.
     * Si se convierte en una fecha valida, la devuelve, sino devuelve NULL
     *
     * @param string|array|\Datetime $date
     *        array $date = array('year' => yyyy, 'month' => mm, 'day' => dd)
     * @return DateTime|NULL
     */
    public static function toDate($date)
    {
        // Si $date es un objeto de la clase DateTime, simplemente los devolvemos
        if ($date instanceof \DateTime) {
            return $date;
        } else {
            /**
             * Si $date es un arreglo, lo convertimos en string para asegurarnos de
             * que funcione correctamente
             */
            if (is_array($date)) {
                if($date['year'] && $date['month'] && $date['day'])
                    $date = str_pad($date['year'], 4, "0", STR_PAD_LEFT) . "-" .
                            str_pad($date['month'], 2, "0", STR_PAD_LEFT) . "-" .
                            str_pad($date['day'], 2, "0", STR_PAD_LEFT);
            }

            /**
             * Verificamos que $date sea un string
             */
            if ( is_string($date) ) {
                /**
                 * Comprobamos que $date tenga el formato yyyy-mm-dd, guardamos las
                 * coincidencias y verificamos que mm y dd esten entre 1 y 12
                 */
                if (preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $date, $devolucion) &&
                    (($devolucion['2'] > 0) && ($devolucion['2'] <= 12)) &&
                    (($devolucion['3'] > 0) && ($devolucion['3'] <= 31))
                   ) {
                    return new \DateTime($date);
                } else {
                    return NULL;
                }
            } else {
                return NULL;
            }
        }
    }

    /**
     * Determina si es un día hábil
     *
     * @param string|\Datetime $date
     *        array $date = array('year' => yyyy, 'month' => mm, 'day' => dd)
     * @return bool
     */
    public static function isWorkDay($date)
    {
        // Nos aseguramos de que sea ingresado una fecha
        $date = self::toDate($date);

        if( !($date instanceof \Datetime) ) {
            return false;
        }

        if($date->format('N') == 6 || $date->format('N') == 7) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returna el siguiente día hábil
     * @param string|\Datetime $date
     *        array $date = array('year' => yyyy, 'month' => mm, 'day' => dd)
     * @return \DateTime
     */
    public static function nextWorkDay($date)
    {
        $date = self::toDate($date);

        if( !($date instanceof \Datetime) ) {
            $date = new \DateTime();
        }

        do {
            $date->modify('+1 day');
        } while ( !self::isWorkDay($date) );

        return $date;
    }
}
