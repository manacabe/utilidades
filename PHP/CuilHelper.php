<?php
/**
 * Anacabe, Martín <anacabe.martin@gmail.com>
 * Creción: 2015-08-06 17:00:00
 */

class CuilHelper
{
    /**
     * Determina si un CUIL/CUIT ingregaso es válido
     *
     * Si no se registran errores, se devuelvo un arreglo con la 'errors' con un arreglo vacio.
     *
     * Si ocurrio algún problema con la validación se retorna un arreglo con la clave 'errors':
     *  1) Si el CUIL/CUIT tiene una longitud distinta de 11, o contiene caracteres que no son números, al arreglo errors se le agrega un arreglo con clave 'cuil', el cual a su vez posee un arreglo con claves 'errorCode', y 'message' (ver el metodo validarNúmero)
     *  2) Si en el punto 1*, no se registraron errores, se verifica que el CUIL/CUIT empiece correctamente. Si no comienza como uno de los valores permitido según sea la persona (masculino, femenino, persona juridica), entonces, en el arreglo errors, se le agrega un arreglo con clave 'cuil', el cual a su vez, posee dos claves 'errorCode' => 3, y 'message'.
     *  3) Si en el punto 2*, no se registraron errores, se intenta generar la sumatoria para calcular el dígito verificador. Si hubo algun problema con los campos documento y/o tipo de persona que figuran dentro del cuil, se agregaran las claves doc/type, los cuales a su vez son un arreglo devuelto por validarDocumento y validarTipo.
     *  4) Si en el punto 3*, no se registraron errores, calculamos el dígito verificador y lo comparamos con el ingresado dentro del CUIL/CUIT. Si el dígito verificador no coincide, se agrega la clave al arreglo 'cuil', la cual a su vez es un arreglo con claves 'errorCode' => 4, 'message' => 'ABC'.
     *
     * @return array
     */
    public static function validar($cuil)
    {
        // Limpiamos el cuil de los guioes
        $cuil = trim(str_replace('-', '', $cuil));

        $result = array('errors' => array());
        $errorCuil = self::validarNumero($cuil, 11);

        // Comprobamos que el cuil este bien formado
        if( empty($errorCuil) )
        {
            // Separamos el cuil en tt-dddddddd-v
            $cuil = array(
                'tipo' => substr($cuil, 0, 2),
                'documento' => substr($cuil, 2, 8),
                'verificador' => substr($cuil, 10, 1)
            );

            // Verificamos que este dentro de los comienzos válidos
            if (
                in_array(
                    $cuil['tipo'],
                    array(20, 23, 24, 27, 30, 33, 34)
                )
            )
            {
                $sumatoria = self::sumatoria($cuil['documento'], $cuil['tipo']);
                if( empty($sumatoria['errors']) )
                {
                    $verificador = self::digitoVerificador($sumatoria['result']);

                    // El dígito de verificación ingresado es incorrecto
                    if($cuil['verificador'] != $verificador)
                        $result['errors']['cuil'] = array('errorCode' => 4, 'message' => "El dígito verificador ingresado, no corresponde con el dígito verificador calculado");
                }
                else{
                    // Errores con el documento o el tipo
                    $resul['errors'] = $sumatoria['errors'];
                }
            }
            else {
                // El CUIL/CUIT es inválido por no empezar con los dígitos válidos
                $result['errors']['cuil'] = array('errorCode' => 3, 'message' => "El CUIL/CUIT no empieza con los números permitidos");
            }
        }
        else {
            // CUIL/CUIT mail formado
            $result['errors'] = $errorCuil;
        }

        return $result;
    }

    /**
     * Generar un CUIL/CUIT válido, a partir del documento y del sexo ingresado
     *
     * Si no se registran errores, se devuelvo un arreglo con dos claves, 'errors' y 'result'. En la clave errors habrá un arreglo vacio, y en result, como es de esperarse, el CUIL/CUIT compuesto sólo por números.
     * Si ocurrio algún problema con la validación del documento o el tipo de persona sólo se retorna un arreglo con la clave 'errors':
     *  1) Si hubo errores con el documento, se agrega a al arreglo a la clave errors con clave doc
     *          $result['errors']['doc'] = array('errorCode' => #, 'message' => 'ABC' )
     *  2) Si hubo errores con el tipo de persona, se agrega a al arreglo a la clave errors con clave doc
     *          $result['errors']['type'] = array('errorCode' => #, 'message' => 'ABC' )
     *  Ambas claves pueden aparecer en el arreglo simultaneamente
     *
     * @param string $documento
     *          Debe tener una longitud de 8 caracteres numéricos.
     * @param string $sexo
     *          Sexo puede ser 'f' => femenino, 'm' => masculino, 'o' => sociedades o empresas.
     */
    public static function generar($documento, $sexo)
    {
        $sexo = strtolower($sexo);
        if( $sexo == 'm' ){
            $tipo = '20';
        }
        else if( $sexo == 'f' ){
            $tipo = '27';
        }
        else
            $tipo = '30';

        $documento = trim((string)$documento);

        $result = self::sumatoria($documento, $tipo);
        if( empty($result['errors']) )
        {
            $verificador = self::digitoVerificador($result['result']);
            $result['result'] = $tipo . $documento . $verificador;
        }

        return $result;
    }

    /**
     * Genera la sumatoria necesaria para calcular el dígito verificador
     *
     * @var integer $documento
     * @var integer $tipo
     *
     * Si no se registran errores, se devuelvo un arreglo con dos claves, 'errors' y 'result'. En la clave errors habrá un arreglo vacio, y en result, como es de esperarse, el total de las sumas parciales.
     * Si ocurrio algún problema con la validación del documento o el tipo de persona sólo se retorna un arreglo con la clave 'errors':
     *  1) Si hubo errores con el documento, se agrega a al arreglo a la clave errors con clave doc
     *          $result['errors']['doc'] = array('errorCode' => #, 'message' => 'ABC' )
     *  2) Si hubo errores con el tipo de persona, se agrega a al arreglo a la clave errors con clave doc
     *          $result['errors']['type'] = array('errorCode' => #, 'message' => 'ABC' )
     *  Ambas claves pueden aparecer en el arreglo simultaneamente
     *
     *
     * @return array
     */
    public static function sumatoria($documento, $tipo)
    {
        // Limpio los campos ingresados
        $tipo = trim((string) $tipo);
        $documento = trim((string) $documento);

        // Corroboro si hay algun tipo de error con el documento o el tipo de CUIL/CUIT a generar
        $comprobacion = array(
            'doc'   => self::validarDocumento($documento),
            'type'  => self::validarTipo($tipo)
        );

        $result = array('errors' => array());

        // Registro si hay errores en el documento
        if( !empty($comprobacion['doc']) )
            $result['errors']['doc'] = $comprobacion['doc'];

        // Registro si hay errores en el tipo de persona
        if( !empty($comprobacion['type']))
            $result['errors']['type'] = $comprobacion['type'];

        // Si no hay errores procedemos a realizar las sumas parciales
        if( empty($result['errors']))
        {
            // Hacemos las sumas parciales para determinar luego el dígito verificador
            $multiplicadores = array('3', '2', '7','6', '5', '4', '3', '2');
            $result['result'] = $tipo['0'] * 5  + $tipo['1'] * 4;

            for($i = 0; $i < 8 ; ++$i) {
                $result['result'] += $documento[$i] * $multiplicadores[$i];
            }
        }

        return $result;
    }

    /**
     * Calcula el dígito verificador a partir de un entero
     *
     * @var integer $sumatoria
     *
     * @return integer
     */
    public static function digitoVerificador($sumatoria)
    {
        $resto = $sumatoria % 11;

        // Obtenemos el dígito verificador
        switch ($resto) {
            case '0':
                $verificador = 0;
                break;
            case '1':
                $verificador = 9;
                break;
            default:
                $verificador = 11 - $resto;
                break;
        }

        return $verificador;
    }

    /**
     * Determina si un documento tiene la longitud exacta y si esta compuesto
     * correctamente (########)(8)
     *
     * Si el número es válido, se retorna un arreglo vacio.
     * Si el número es inválido retorna un arreglo con las claves 'errorCode' y 'messaje':
     *      1 - Si la cantidad de caracteres de la cadena es distinta de la cantidad a validar, errorcode es igual a 1.
     *      2 - Si la cantidad de caracteres de la cadena es posee algun caracter NO numérico, errorcode es igual a 2.
     *
     * @param integer $documento
     *
     * @return array
     */
    public static function validarDocumento($documento)
    {
        // Limpio la cadena
        $documento = trim((string)$documento);

        // Retorno los errores encontrados
        return self::validarNumero($documento, 8);
    }

    /**
     * Determina si un tipo tiene la longitud exacta y si esta compuesto
     * correctamente (##)(2)
     *
     * Si el número es válido, se retorna un arreglo vacio.
     * Si el número es inválido retorna un arreglo con las claves 'errorCode' y 'messaje':
     *      1 - Si la cantidad de caracteres de la cadena es distinta de la cantidad a validar, errorcode es igual a 1.
     *      2 - Si la cantidad de caracteres de la cadena es posee algun caracter NO numérico, errorcode es igual a 2.
     *
     * @param integer $tipo
     *
     * @return array
     */
    public static function validarTipo($tipo)
    {
        // Limpio la cadena
        $tipo = trim((string)$tipo);

        // Retorno los errores encontrados
        return self::validarNumero($tipo, 2);
    }

    /**
     * Determina si un tipo tiene la longitud exacta y si esta compuesto
     * correctamente (#)(longitud)
     *
     * @param integer $numero
     * @param integer $longitud
     *
     * @return array
     *
     * Si el número es válido, se retorna un arreglo vacio.
     * Si el número es inválido retorna un arreglo con las claves 'errorCode' y 'messaje':
     *      1 - Si la cantidad de caracteres de la cadena es distinta de la cantidad a validar, errorcode es igual a 1.
     *      2 - Si la cantidad de caracteres de la cadena es posee algun caracter NO numérico, errorcode es igual a 2.
     */
    public static function validarNumero($numero, $longitud)
    {
        // Limpio la cadena
        $numero = trim((string)$numero);

        // Variable donde se guardarán el/los errores
        $errors = array();

        // Guardo la longitud
        $longTemp = strlen($numero);

        // Compruebo el largo de la cadena y registro un error si no es del largo correcto
        if($longTemp != $longitud)
            $errors = array('errorCode' => 1, 'message' => "La longitud no es $longitud");
        else
        {
            // Recorro la cadena hasta encontrar el primer caracter que no es un dígito
            for($i = 0; $i < $longTemp; ++$i)
            {
                // Si no es un número, registro un error
                if( !is_numeric($numero[$i]) ){
                    $errors = array('errorCode' => 2, 'message' => "No esta compuesto solo de números");
                    break;
                }
            }
        }

        // Devuelvo el error encontrado
        return $errors;
    }
}
